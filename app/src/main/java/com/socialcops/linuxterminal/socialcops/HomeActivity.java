package com.socialcops.linuxterminal.socialcops;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;

public class HomeActivity extends AppCompatActivity {
    private VideoAdapter videoAdapter;
    private List<String> url;
    private FloatingActionButton fab;
    private SharedPreferences prefs;
    private Set<String> set;
    private SharedPreferences.Editor edit;

    /*method for testing connectivity status*/
    private static boolean connectivityStatus(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        prefs = this.getSharedPreferences("userURL", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();

        set = prefs.getStringSet("UserURL", null);

        if (set == null) {
            set = new HashSet<>();
            url = new ArrayList<>();

            url.add(getString(R.string.hardcodedURL));
            set.addAll(url);
            edit.putStringSet("UserURL", set);
            edit.apply();
        } else {
            url = new ArrayList<>(set);
            Collections.reverse(url);

        }


        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        videoAdapter = new VideoAdapter(this, url);
        recyclerView.setAdapter(videoAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        fab = findViewById(R.id.floatingActionButton);
        fabDialog();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });

        showIntro(fab);

    }

    /* method for adding another test videos*/
    private void fabDialog() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater li = LayoutInflater.from(HomeActivity.this);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        HomeActivity.this);

                alertDialogBuilder.setView(promptsView);

                final EditText userInput = promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        if (connectivityStatus(HomeActivity.this)) {
                                            testURL(userInput.getText().toString());
                                        } else {
                                            showSnackBar(0);
                                        }


                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });
    }

    /* method for testing URL*/
    private void testURL(final String inputURL) {
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                try {
                    boolean validURL = URLUtil.isValidUrl(inputURL);
                    if (validURL) {
                        URL ur = new URL(inputURL);

                        URLConnection u = ur.openConnection();
                        String type = u.getHeaderField("Content-Type");
                        Log.d("type", type);
                        if (type.equals("video/mp4")) {
                            insertItem(inputURL);
                        } else {
                            showSnackBar(1);
                        }
                    } else {
                        showSnackBar(1);
                    }
                    Looper.loop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();


    }

    /*method for inserting new video into sharedprefrence and update recyclerview*/
    private void insertItem(final String inputURL) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                url.add(inputURL);
                set = new HashSet<>();

                set.addAll(url);
                edit = prefs.edit();
                edit.putStringSet("UserURL", set);
                edit.apply();
                videoAdapter.notifyItemInserted(url.size() - 1);
            }
        });


    }

    private void showSnackBar(int status) {
        if (status == 0) {
            Snackbar.make(findViewById(R.id.parentView), "Internet Connectivity fail", Snackbar.LENGTH_LONG)
                    .show();
        } else {
            Snackbar.make(findViewById(R.id.parentView), "Invalid URL", Snackbar.LENGTH_LONG)
                    .show();
        }

    }


    private void showIntro(View view) {
        new MaterialIntroView.Builder(this)
                .enableDotAnimation(true)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.ALL)
                .setDelayMillis(200)
                .enableFadeAnimation(true)
                .performClick(true)
                .setInfoText("Add more Test video URLS")
                .setTarget(view)
                .setUsageId("INTRO_FOCUS_1") //THIS SHOULD BE UNIQUE ID
                .show();
    }

}



