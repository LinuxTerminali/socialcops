package com.socialcops.linuxterminal.socialcops;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;



public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {

    private final Context mContext;
    private final List<String> urls;

    public VideoAdapter(Context mContext, List<String> urls) {
        this.mContext = mContext;
        this.urls = urls;
    }

    @Override
    public VideoAdapter.VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v1 = inflater.inflate(R.layout.videolayout, parent, false);
        viewHolder = new VideoHolder(v1);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        final String url = urls.get(position);

        holder.title.setText("Test Video #" + position);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PlayerActivity.class);
                intent.putExtra("url", url);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final ImageView thumbnail;
        public final LinearLayout linearLayout;

        public VideoHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.videotitle);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            linearLayout = itemView.findViewById(R.id.videolinear);

        }


    }


}

